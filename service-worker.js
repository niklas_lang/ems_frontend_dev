self.addEventListener('install', function(event) {
  	// Perform install steps
  	var CACHE_NAME = 'my-site-cache-v1';
	var urlsToCache = [
	  '/',
	  '/service-worker.js',
	  'src/my-app.html',
	  'src/my-admin-panel-view.html',
	  'src/my-campus-view.html',
	  'src/my-documents-view.html',
	  'src/my-newsfeed-view.html',
	  'src/my-profile-view.html',
	  'src/my-std-profile-view.html',
	  'src/my-students-view.html',
	  'bower_components/paper-fab/paper-fab.html',
	  'custom_elements/my-img-crop/my-img-crop.html',
	  'custom_elements/document-item/document-item.html',
	  'custom_elements/folder-item/folder-item.html',
	  'images/anonymous.jpg'



	];
	  // Perform install steps
	  event.waitUntil(
	    caches.open(CACHE_NAME)
	      .then(function(cache) {
	        console.log('Opened cache');
	        return cache.addAll(urlsToCache);
	      })
	  );
});
self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request)
      .then(function(response) {
        // Cache hit - return response
        if (response) {
          return response;
        }
        return fetch(event.request);
      }
    )
  );
});
