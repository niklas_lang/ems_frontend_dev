# README #

This README will help you run the front-end portion of this project properly on your own machine.

### What is this repository for? ###

* Educational Management System

### How do I get set up? ###

* Install the NPM package manager here: https://www.npmjs.com/
* Install the Polymer CLI by running this command in the terminal/ cmd window: `npm install -g polymer-cli`
* Clone this EMS_frontend_dev repository to your system.
* navigate inside of the cloned repository, and type this in the teminal/cmd window: `Polymer serve --open`

### If there are any questions feel free to contact us ###

#### Front-end: ####
* Dylan Carver - dylancarv@gmail.com
* Niklas Lang - contis2908@gmail.com
#### Back-end: ####
* Alexander Stevens - alexanderbilliestevens@gmail.com